(function(app) {

  class ProductsComponent {
    constructor() {
      this.tableHeader = "Products";
    }

    getProducts() {
      return [
        {name: 'dog house', price: 333}, 
        {name: 'cat house', price: 555}
      ];
    }
  }

  ProductsComponent.annotations  = ng.core.Component({
    selector: 'ng-products',
    templateUrl: 'app/products/products.component.html'
  });

  app.ProductsComponent = ProductsComponent;

})(window.app || (window.app = {}));