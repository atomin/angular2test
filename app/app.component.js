(function(app) {

  class AppComponent {
    constructor() {
    }
  }

  AppComponent.annotations = ng.core.Component({
    selector: 'my-app',
    templateUrl: 'app/app.component.html',
    directives: [app.ProductsComponent]
  });

  app.AppComponent = AppComponent;

})(window.app || (window.app = {}));